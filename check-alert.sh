#!/bin/bash
#
# Use this sript to read events from the "interaction-alert" topic using the console consumer
#

docker-compose exec schema_registry \
  kafka-avro-console-consumer --topic interaction-alert \
  --bootstrap-server broker:9092 \
  --property schema.registry.url=http://schema_registry:8081

