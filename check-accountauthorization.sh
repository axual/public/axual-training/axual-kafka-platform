#!/bin/bash
#
# Use this script to read events from the payments-accountauthorization topic using the console consumer
#

docker-compose exec schema_registry \
  kafka-avro-console-consumer --topic payments-accountauthorization \
  --bootstrap-server broker:9092 --from-beginning \
  --property schema.registry.url=http://schema_registry:8081

