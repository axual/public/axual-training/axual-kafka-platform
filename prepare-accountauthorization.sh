#!/bin/sh
#
# Use the following command to create the payments-accountauthorization compaction topic
#

docker-compose exec broker \
  kafka-topics --create --topic payments-accountauthorization \
  --partitions 12 --replication-factor 1 \
  --if-not-exists --config cleanup.policy=compact \
  --zookeeper zookeeper:2181

